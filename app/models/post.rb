class Post < ActiveRecord::Base
  #This esnures that comments are deleted with post
  has_many :comments, dependent: :destroy
  #Does validation to ensure title is not blank and can't be less than 5 chars
  validates :title, presence: true, length: { minimum: 5 }
  #Checks to see if body is blank
  validates :body, presence: true
end
